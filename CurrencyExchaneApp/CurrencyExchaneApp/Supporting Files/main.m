//
//  main.m
//  CurrencyExchaneApp
//
//  Created by Leonardo Madrigal on 3/15/18.
//  Copyright © 2018 konradgroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
